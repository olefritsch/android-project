package se.iths.ole.androidproject;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by iths on 2015-11-24.
 */
public class ProfileCursorAdapter extends CursorAdapter{

    public ProfileCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.profile_list_item, null);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView nameText = (TextView)view.findViewById(R.id.name_textview);
        String name = cursor.getString(cursor.getColumnIndex("name"));
        nameText.setText(name);

    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


}

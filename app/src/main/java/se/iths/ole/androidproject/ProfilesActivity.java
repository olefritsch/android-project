package se.iths.ole.androidproject;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class ProfilesActivity extends AppCompatActivity {

    private DBHelper dbHelper;
    private EditText enterName;
    private ListView profileListView;
    private Cursor profiles;
    private boolean remove = false;
    private SimpleCursorAdapter adapter;
    private String[] from;
    private int[] to;
    private Button deleteProfileButton;
    private static final String PROFILES_REQUEST_TEXT = "PROFILE REQUEST";
    public static final String BACK_TO_MAIN_KEY = "backToMain";
    public static final String FROM_NUMBER_OF_PLAYERS_KEY = "fromNumberOfPlayers";
    public static final int PROFILES_REQUEST_CODE = 1;
    public static final int NUMBER_OF_PLAYERS_REQUEST_CODE = 2;
    private String playerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.profiles_title));
        setContentView(R.layout.activity_profiles);

        dbHelper = new DBHelper(this);
        enterName = (EditText) findViewById(R.id.enter_name);
        profileListView = (ListView) findViewById(R.id.profiles_listview);
        profiles = dbHelper.getProfiles();
        from = new String[]{DBHelper.NAME_KEY};
        to = new int[]{R.id.name_textview};
        adapter = new SimpleCursorAdapter(this, R.layout.profile_list_item, profiles, from, to, 0);
        profileListView.setAdapter(adapter);
        deleteProfileButton = (Button) findViewById(R.id.remove_profile_button);

        profileListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = getIntent();
                boolean backToMain = intent.hasExtra(BACK_TO_MAIN_KEY);
                boolean fromNumberOfPlayers = intent.hasExtra(FROM_NUMBER_OF_PLAYERS_KEY);

                if (remove) {
                    dbHelper.deleteProfile(id);
                    profiles = dbHelper.getProfiles();
                    activateRemoveBoolean(view);
                    refreshProfileList();
                    ProfileHelper.update();
                } else if (fromNumberOfPlayers) {
                    ProfileHelper.getInstance().setActiveProfile2(position);
                    goTo(1);
                } else if (!backToMain){
                    ProfileHelper.getInstance().setActiveProfile(position);
                    goTo(2);
                } else {
                    ProfileHelper.getInstance().setActiveProfile(position);
                }

                finish();

            }
        });
    }

    public void addProfile(View view) {
        playerName = enterName.getText().toString();
        if (playerName.equals("")) {
            Toast.makeText(getApplicationContext(), "Set name before creating new profile", Toast.LENGTH_SHORT).show();
        } else if (playerName.length() < 3) {
            Toast.makeText(getApplicationContext(), "Name needs to have 3 or more characters", Toast.LENGTH_SHORT).show();
        } else {
            dbHelper.addProfileToDB(playerName);
            profiles = dbHelper.getProfiles();
            Toast.makeText(getApplicationContext(), playerName + " has been created!", Toast.LENGTH_SHORT).show();
         /*   ProfileHelper.update(); */
        }
        enterName.setText("");
        refreshProfileList();
    }

    private void refreshProfileList() {
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.profile_list_item, profiles, from, to, 0);
        profileListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void activateRemoveBoolean(View view) {
        if(remove) {
            remove=false;
            deleteProfileButton.setTextSize(14f);
        } else {
            remove=true;
            deleteProfileButton.setTextSize(20f);
        }
    }

    private void goTo(int destinationCode) {
        Intent intent;
        if (destinationCode == 1) {
            intent = new Intent(this, Category.class);
        } else if (destinationCode == 2) {
            intent = new Intent(this, NumberOfPlayersActivity.class);
        } else {
            intent = new Intent(this, MainActivity.class);
        }
        startActivity(intent);
    }
}
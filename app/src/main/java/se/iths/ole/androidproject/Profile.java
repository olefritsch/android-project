package se.iths.ole.androidproject;

/**
 * Created by iths on 2015-11-20.
 */
public class Profile {

    private String name;
    private int highscore;

    public Profile(String name) {
        this.name = name;
        highscore = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHighscore() {
        return highscore;
    }

    public void checkHighscore(int score) {
        if(highscore < score) {
            highscore = score;
        }
    }

    @Override
    public String toString() {
        return name;
    }
}

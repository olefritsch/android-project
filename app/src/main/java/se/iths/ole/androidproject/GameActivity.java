package se.iths.ole.androidproject;

import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class GameActivity extends AppCompatActivity {

    public static final String DIALOG_FRAGMENT = "DIALOG_FRAGMENT";
    public static final String CATEGORY_BOOLEAN = "CATEGORY_BOOLEAN";
    public static final String PROFILES = "Profiles";
    public static TextView activePlayerTV;
    public static String nextPlayer;
    public static CountDownTimer timer;

    private DBHelper dbHelper;
    private String player1;
    private String player2;
    private Game p1Game;
    private Game p2Game;
    private Game activeGame;
    private boolean mpGame;
    private int count;
    private Question[] questions;
    private TextView answer1;
    private TextView answer2;
    private TextView answer3;
    private TextView answer4;
    private int time;
    private Button imgButton1;
    private Button imgButton2;
    private Button imgButton3;
    private Button imgButton4;
    private MediaPlayer mySound;
    private MediaPlayer mySound2;
    private int[] imgArray;
    private ImageView timerAnimation;
    private NextPlayerDialogFragment nextPlayerDialogFragment;
    private int timePassed;
    private int correctGuesses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);
        dbHelper = new DBHelper(this);

        activePlayerTV = (TextView) findViewById(R.id.view_player);

        imgButton1 = (Button) findViewById(R.id.answer1);
        imgButton2 = (Button) findViewById(R.id.answer2);
        imgButton3 = (Button) findViewById(R.id.answer3);
        imgButton4 = (Button) findViewById(R.id.answer4);

        imgArray = new int[]{R.drawable.timer_at_last, R.drawable.timer_last,
                R.drawable.timer0, R.drawable.timer1, R.drawable.timer2,
                R.drawable.timer3, R.drawable.timer4, R.drawable.timer5,
                R.drawable.timer6, R.drawable.timer7, R.drawable.timer8,
                R.drawable.timer9, R.drawable.timer10};

        nextPlayerDialogFragment = new NextPlayerDialogFragment();

        timerAnimation = (ImageView) findViewById(R.id.timer);

        mySound = MediaPlayer.create(this, R.raw.correct);
        mySound2 = MediaPlayer.create(this, R.raw.wrong);

        count = 0;
        questions = setUpQuestion(getIntent().getStringExtra(ResultActivity.CATEGORY_KEY));
        player1 = ProfileHelper.getInstance().getActiveProfile();
        p1Game = new Game(player1, questions);
        activeGame = p1Game;
        timePassed = -10;

        timer();
        displayQuestion();

        if (ProfileHelper.getInstance().isMpGame()) {
            mpGame = true;
            player2 = ProfileHelper.getInstance().getActiveProfile2();
            p2Game = new Game(player2, questions);
            nextPlayer = player1;
            activePlayerTV.setText(activeGame.getPlayer());
            nextPlayerDialogFragment.show(getFragmentManager(), DIALOG_FRAGMENT);
        }else {
            timer.start();
        }
    }

    /**
     * Gets 10 random questions from database.
     *
     * @param category
     * @return questions
     */
    private Question[] setUpQuestion(String category) {
        ArrayList<Question> questionArrayList = new ArrayList<Question>();
        Question[] questions = new Question[10];
        Random random = new Random();
        boolean categoryBoolean = getIntent().getExtras().getBoolean(CATEGORY_BOOLEAN);

        Log.d("DebugCatBoolVal", String.valueOf(categoryBoolean));

        Cursor cursor;
        if (categoryBoolean == true) {
            cursor = dbHelper.getAllPlayerData();
        } else {
            cursor = dbHelper.getQuestions(category);
        }

        if (cursor.moveToFirst()) { //cursor.moveToFirst() är false
            do {
                questionArrayList.add(new Question(category, cursor.getString(2), cursor.getString(3),
                        cursor.getString(4), cursor.getString(5), cursor.getString(6)));
            } while (cursor.moveToNext());
        }

        for (int i = 0; i < 10; i++) { //Puts random questions in an array
            int randomInt = random.nextInt(questionArrayList.size());
            questions[i] = questionArrayList.get(randomInt);
            questionArrayList.remove(randomInt); //Makes sure the same question doesn't appear more than once
        }
        return questions;
    }

    /**
     * Displays questions and shuffle the possible answers.
     */
    private void displayQuestion() {
        String[] answers = questions[count].getAnswers();

        TextView questionTv = (TextView) findViewById(R.id.question);
        answer1 = (TextView) findViewById(R.id.answer1);
        answer2 = (TextView) findViewById(R.id.answer2);
        answer3 = (TextView) findViewById(R.id.answer3);
        answer4 = (TextView) findViewById(R.id.answer4);

        questionTv.setText(questions[count].getQuestion());

        ArrayList<Integer> numbers = new ArrayList<Integer>();
        for (int i = 0; i < 4; i++) {
            numbers.add(i);
            Collections.shuffle(numbers);
        }

        answer1.setText(answers[numbers.get(0)]);
        answer2.setText(answers[numbers.get(1)]);
        answer3.setText(answers[numbers.get(2)]);
        answer4.setText(answers[numbers.get(3)]);
    }

    /**
     * Handles the animation when player answers a question.
     *
     * @param string
     * @param view
     */
    private void guess(String string, View view) {
        timer.cancel();
        timePassed += (12 - time);

        if (activeGame.takeTurn(string, time)) {
            correctGuesses ++;
            view.setBackgroundResource(R.drawable.button_green);
            view.postDelayed(swapImage, 200);
            mySound.start();
        } else {
            view.setBackgroundResource(R.drawable.red_button);
            view.postDelayed(swapImage, 200);
            mySound2.start();
        }

        nextTurn();
    }

    /**
     * Finishes the current turn, resets the timer and displays the next question.
     * In case of final turn, ends game and starts resultActivity.
     */
    private void nextTurn() {
        if (count < 9 || (mpGame && count == 9 && activeGame == p1Game)) {
            if ((mpGame && activeGame == p2Game) || (!mpGame)) {
                count++;
                activeGame = p1Game;
            } else if (mpGame && activeGame == p1Game) {
                activeGame = p2Game;
            }

            if (mpGame) {
                nextPlayerDialogFragment.show(getFragmentManager(), DIALOG_FRAGMENT);
                activePlayerTV.setText(activeGame.getPlayer());
                if(nextPlayer.equals(player2)) {
                    nextPlayer = player1;
                }else {
                    nextPlayer = player2;
                }
            } else {
                timer.start();
            }
            displayQuestion();

        }else{
            endGame();
        }
    }



    /**
     * Listeners for buttons
     *
     * @param view
     */
    public void answer1(View view) {
        guess(answer1.getText().toString(), view);
    }

    public void answer2(View view) {
        guess(answer2.getText().toString(), view);
    }

    public void answer3(View view) {
        guess(answer3.getText().toString(), view);
    }

    public void answer4(View view) {
        guess(answer4.getText().toString(), view);
    }

    /**
     * A timer to keep remaining time for player to answer the question.
     */
    private void timer() {
        if(timer == null) {
            timer = new CountDownTimer(13000, 1000) {
                public void onTick(long millisUntilFinished) {

                    time = (int) millisUntilFinished / 1000;
                    if(time <= 1) {
                        timerAnimation.setImageResource(imgArray[0]);
                    }else if (time == 13) {
                        timerAnimation.setImageResource(imgArray[time-1]);
                    }else {
                        timerAnimation.setImageResource(imgArray[time]);
                    }
                }

                public void onFinish() {
                    activeGame.takeTurn("", 0);
                    nextTurn();
                }
            };
        }
    }

    private void endGame() {
        timer = null;
        Intent oldIntent = getIntent();
        Intent intent = new Intent(this, ResultActivity.class);
        String cat = oldIntent.getStringExtra(ResultActivity.CATEGORY_KEY);

        if (cat == null) {
            cat = PROFILES;
        }

        intent.putExtra(ResultActivity.PLAYER_1_SCORE_KEY, p1Game.getPoints());
        intent.putExtra(ResultActivity.TIME_PASSED_KEY, timePassed);
        intent.putExtra(ResultActivity.CORRECT_GUESSES_KEY, correctGuesses);

        if (mpGame) {
            intent.putExtra(ResultActivity.PLAYER_2_SCORE_KEY, p2Game.getPoints());

            if (p2Game.getPoints() > dbHelper.getProfileHighscore(p2Game.getPlayer(), cat)) {
                dbHelper.updateHighscore(player2, p2Game.getPoints(), cat);
                intent.putExtra(ResultActivity.NEW_HIGHSCORE_KEY, true);
            }
        }

        if (p1Game.getPoints() > dbHelper.getProfileHighscore(p1Game.getPlayer(), cat)) {
            dbHelper.updateHighscore(player1, p1Game.getPoints(), cat);
            intent.putExtra(ResultActivity.NEW_HIGHSCORE_KEY, true);
        }
        if (cat=="Profiles"){
            cat = "All Categories";
        }
        intent.putExtra(ResultActivity.CATEGORY_KEY, cat);
        startActivity(intent);
        finish();
    }

    /**
     * Handles the animation to buttons whether the player quesses right or wrong.
     */
    private Runnable swapImage = new Runnable() {
        @Override
        public void run() {
            imgButton1.setBackgroundResource(R.drawable.answer_button_blue);
            imgButton2.setBackgroundResource(R.drawable.answer_button_blue);
            imgButton3.setBackgroundResource(R.drawable.answer_button_blue);
            imgButton4.setBackgroundResource(R.drawable.answer_button_blue);
        }
    };
}




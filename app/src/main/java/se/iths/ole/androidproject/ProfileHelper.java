package se.iths.ole.androidproject;

import android.content.Context;
import java.util.ArrayList;

/**
 * Created by Ole on 09/12/15.
 */
public class ProfileHelper {
    private static ArrayList<String> names;
    private static int activeProfile;
    private static int activeProfile2;
    private static boolean mpGame;
    private static DBHelper dbHelper;
    private static Context context;
    private static ProfileHelper instance;

    private ProfileHelper() {
        dbHelper = new DBHelper(context);
        update();
        mpGame = false;
    }

    public static ProfileHelper getInstance() {
        if (instance == null) {
            instance = new ProfileHelper();
        }
        return instance;
    }

    public static void setContext(Context c) {
        context = c;
    }

    public static void update() {
        names = dbHelper.getProfileNames();
    }

    public void setActiveProfile(int position) {
        activeProfile = position;
    }

    public void setActiveProfile2(int position) {
        activeProfile2 = position;
        mpGame = true;
    }

    public void setMpGameFalse() {
        mpGame = false;
    }

    public String getActiveProfile() {
        return names.get(activeProfile);
    }

    public String getActiveProfile2() {
        return names.get(activeProfile2);
    }

    public boolean isMpGame() {
        return mpGame;
    }

}

/*package se.iths.ole.androidproject;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PlayerCategories extends AppCompatActivity {

    private DBHelper dbHelper = new DBHelper(this);
    private ListView playerCategoryListView;
    private ArrayList<String> sortedList;
    private Cursor res;
    private ArrayList<Question> questionArrayList;
    private ArrayList<String> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_categories);

        playerCategoryListView = (ListView) findViewById(R.id.player_categories_list_view);
        categories = new ArrayList<String>();
        res = dbHelper.getAllPlayerData();

        //Gets the custom questions.
        while (res.moveToNext()) {
            categories.add(res.getString(1) + "\n");
        }

        //Filters the list, so only one of each category is shown.
        sortedList = new ArrayList<String>();
        for(int i=0; i< categories.size(); i++) {
            if(!sortedList.contains(categories.get(i))) {
                sortedList.add(categories.get(i));
            }
        }

        Log.d("sortedListDebug", String.valueOf(sortedList));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sortedList);
        playerCategoryListView.setAdapter(adapter);

        playerCategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Cursor cursor = dbHelper.getPlayerQuestions(categories.get(position));

                questionArrayList = new ArrayList<>();
                Log.d("Debug res.Value", String.valueOf(res.moveToFirst()));
                if(res.moveToFirst()) {
                    do {
                        questionArrayList.add(new Question(res.getString(1),
                                                            res.getString(2),
                                                            res.getString(3),
                                                            res.getString(4),
                                                            res.getString(5),
                                                            res.getString(6)));
                    } while (res.moveToNext());
                }

                Log.d("Debug listSize", String.valueOf(questionArrayList.size()));
                if(questionArrayList.size()<10) {
                    Toast.makeText(PlayerCategories.this, R.string.too_few_questions_toast_text, Toast.LENGTH_SHORT).show();
                } // else {
                    Intent intent = new Intent(view.getContext(), GameActivity.class);
                    System.out.println(sortedList.get(position));
                    intent.putExtra(GameActivity.CATEGORY_KEY, sortedList.get(position).toString().toUpperCase());
                    intent.putExtra(GameActivity.CATEGORY_BOOLEAN, true);
                    startActivity(intent);
                  // }
            }
        });
    }
}
*/
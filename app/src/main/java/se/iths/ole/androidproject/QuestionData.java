package se.iths.ole.androidproject;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Ole on 26/11/15.
 */
public class QuestionData {
    private static final String FILM = "Film";
    private static final String SPORT = "Sport";
    private static final String MUSIC = "Music";
    private static final String GAMES = "Games";
    private static final String GEOGRAPHY = "Geography";

    private ArrayList<Question> questions = new ArrayList<>();

    /*
    * Creates a new QuestionData instance and adds all data to a single ArrayList.
     */
    public QuestionData() {
        /*
        Music
         */
        addData(MUSIC, "Who originally wrote the song All along the watchtower?", "Bob Dylan",
                "Andy Warhol", "Jimi Hendrix", "Janis Joplin");
        addData(MUSIC,"The 27 Club is a term that refers to a number of popular musicians who died at age 27." +
                " Who does not belong here?","Buddy Holly","Jimi Hendrix","Janis Joplin","Kurt Cobain");
        addData(MUSIC,"What band has the second best selling album of all time?","ACDC",
                "Pink Floyd","Whitney Houston","Meat Loaf");
        addData(MUSIC,"What is the Beatles first album called?","Please Please Me","Help",
                "With The Beatles","Rubber Soul");
        addData(MUSIC,"What’s the name of Foo Fighter's frontman and former drummer for the band Nirvana?",
                "Dave Grohl","Krist Novoselic","Pat Smear","Taylor Hawkins" );
        addData(MUSIC,"The Toxic Twins is a nickname given to?","Steven Tyler and Joe Perry",
                "Mick Mars and Nikki Sixx","Axl Rose and Slash","Jimmy Page and Robert Plant");
        addData(MUSIC,"What is Lady Gaga’s real name?","Stefani Gernamota","Stefani Banic",
                "Stefani Jordan", "Stefani Olsson");
        addData(MUSIC,"What’s the name of the singer in the Swedish band Kent?","Jocke Berg",
                "Stefan Larsson","Adam Gren", "Carl Johnson");
        addData(MUSIC,"Jimmy Page is an English musician who formed what rock band in 1968?",
                "Led Zeppelin","Dire Straits", "Rush","Deep Purple");
        addData(MUSIC,"In which American city was Elvis discovered dead in 1977?",
                "Memphis","Nashville","Louisevill","New Orleans");
        addData(MUSIC,"The Stratocaster is a model of which guitar maker?",
                "Fender","Gibson","Ibanez","Jackson");
        addData(MUSIC,"Which Rolling Stones guitarist died in a swimming pool in 1969?",
                "Brian Jones","Bill Wyman","Ian Stewart","Mick Taylor");
        addData(MUSIC,"Frank Sinatra, Dean Martin, and Sammy Davis Junior were among several" +
                        " entertainers given what collective nickname?",
                "The Rat Pack","The Classics","The Originals","the Goodies");
        addData(MUSIC,"What record label did the Beatles establish?",
                "Apple", "Blue Dog Records","Blue Cat Records","Eagle Records");
        addData(MUSIC,"Which American rock'n'roll star caused controversy when he married a young teenager?",
                "Jerry Lee Lewis", "Little Richard","Chuck Berry","Fats Domino");
        addData(MUSIC,"What was the 1st video ever played on MTV?",
                "Video Kill The Radio Start","Back in Black","Nothing's Gonna Stop Us Now", "Should I Stay OR Should I Go.");
        addData(MUSIC,"Who was the youngest sibling in the Jackson 5?",
                "Randy","Michael","Tito","Marlon");
        addData(MUSIC, "R&B superstar Marvin Gaye was shot to death by which family member?",
                "His Father", "His Mother", "His Uncle","His Brother");
        addData(MUSIC,"What year did 2pac die?",
                "1996","1999","1991","1993");
        addData(MUSIC,"What did Def Leppard drummer Rick Allen lose in a 1984",
                "An arm","His legs","His membership","His girlfriend" );
        /*
        Sports
         */
        addData(SPORT,"In what month is the traditional New Tork City Marathon?",
                "November","April","May","August");
        addData(SPORT,"Who won Tour de France 1998?",
                "Pantani","Virenque","Armstrong","Ullrich");
        addData(SPORT,"India reached the final of the Davis Cup for the first time in...?",
                "1966","1962","1972","1974");
        addData(SPORT,"Germany won the final match of the 1990 Football World Cup. Which team did they defeat?",
                "Argentina","Italy","France","England");
        addData(SPORT,"How many medals did Sweden achieve during the World Athletics Championships 2013?",
                "1","3","5","6");
        addData(SPORT,"Which nation has won the World Championships of ice hockey the most times?",
                "Canada","Sweden","Russia","USA");
        addData(SPORT,"Where is the Millennium Stadium?",
                "Cardiff","Oslo","London","Paris");
        addData(SPORT,"How many chess pieces does the chessboard have?",
                "32","30","28","26");
        addData(SPORT,"How many Olympic Gold has the legendary ice hockey player Peter Forsberg won?",
                "2","1","3","4");
        addData(SPORT,"Who unexpectedly won the 2009 US Open women's championship?",
                "Kim Clijsters","Maria Sharapova","Serena William","Justine Henin");
        addData(SPORT,"In what year did India win its first Olympic hockey gold?",
                "1928","1932","1936","1940");
        addData(SPORT,"Who won the 2004 Golf US Masters?",
                "Phil Mickelson","Chris DiMarco","Mike Weir","Ernie Els");
        addData(SPORT, "What color does the tablecloth usually have for Snooker?",
                "Green", "Blue", "Red", "Black");
        addData(SPORT, "Which team won the 2015 Super Bowl?",
                "Patriots", "Seahawks", "Packers", "Cowboys");
        addData(SPORT, "What was the final score in the semifinals of the Football World Cup 2014 between Brazil and Germany?",
                "1 - 7", "3 - 1", "2 - 3", "0 - 1");
        /*
        Games
         */
        addData(GAMES,"Who is the creator of the Super Mario franchise?",
                "Shigeru Miyamoto","Hiroshi Yamauchi","Sekiryo Kaneda","Reggie Fils-Aimé");
        addData(GAMES,"Which of the following games is only available on Playstayion 4?",
                "Until Dawn","Halo 5: Guardians","Mario Party 6","Duck Hunt");
        addData(GAMES,"Which company created the Xbox?",
                "Microsoft","Sony","Nintendo","SEGA");
        addData(GAMES,"Wich is the best-selling video game console of all time?",
                "Playstation 2","Nintendo Wii","Nintendo DS","Sega Genesis");
        addData(GAMES,"What is E3?",
                "A game convention","A game console","A character","A dragon");
        addData(GAMES,"SEGA owns the rights to a fast, blue hedgehog with red shoes. What's his name?",
                "Sonic","Spyke","Speedy","Sega");
        addData(GAMES,"Which of the following was not a starter Pokémon in Pokémon Red/Blue?",
                "Pikachu","Squirtle","Bulbasaur","Charmander");
        addData(GAMES,"Swedish video game designer Markus Persson is famous for creating one game. Which one?",
                "Minecraft","Runescape","Guild Wars","Angry Birds");
        addData(GAMES,"Which of the following franchises has not appeared in any Angry Birds game?",
                "Power Rangers","Transformers","Star Wars","Rio");
        addData(GAMES,"What is the name of Super Mario's green-clad brother?",
                "Luigi","Giovanni","Leonardo","Ezio");
        addData(GAMES,"When was the Xbox One released in the United States of America?",
                "2013","2012","2014","2011");
        addData(GAMES,"What does RPG mean?",
                "Role-playing game","Radio-powered game","Retro-play game","Reprogramming");
        addData(GAMES,"What is Hearthstone?",
                "A trading-card game","A sports game","A racing game","An action game");
        addData(GAMES,"In which game is Master Chief the protagonist?",
                "Halo: Combat Evolved","Metal Gear Solid","Mario Kart DS","Killzone");
        addData(GAMES,"What whas the Game Boy released?",
                "1989","1999","1985","1979");

        /*
        Geography
         */
        addData(GEOGRAPHY, "How many states are part of the USA?", "50", "51", "52", "53");
        addData(GEOGRAPHY, "What is the capital of Iceland?", "Reykjavik", "Kópavogur", "Akranes", "Nuuk");
        addData(GEOGRAPHY, "Which of the following borders 14 countries?", "China", "Sudan", "Brazil", "DRC");
        addData(GEOGRAPHY, "Which of the following has the highest population density?", "Macau", "Monaco", "Singapor", "Gibraltar");
        addData(GEOGRAPHY, "Which of the following countries does not lie along the equator?", "Panama", "Kenya",
                "Uganda", "Indonesia");
        addData(GEOGRAPHY, "Which city has the longitudanal coordinate 0?", "London", "New York", "Paris", "Melbourne");
        addData(GEOGRAPHY, "What is the second largest country in the world in terms of surface area?", "Canada",
                "Russia", "China", "Australia");
        addData(GEOGRAPHY, "What is the deepest known point in the Mariana Trench?", "~11km", "~10km", "~12km", "~9km");
        addData(GEOGRAPHY, "The great Victoria Desert is located in...?", "Australia", "West Africa",
                "North Africa", "South America");
        addData(GEOGRAPHY, "Which of the following is tropical grassland?", "Savannah", "Taiga", "Pampas", "Prairies");
        addData(GEOGRAPHY, "What is largest continent in the world?", "Asia", "Africa", "Antarctica", "North America");
        addData(GEOGRAPHY, "The artic cirlce lies how many degrees above the equator?", "66°", "60°", "70°", "72°");
        addData(GEOGRAPHY, "The Bermuda Triangle lies of the coast of..?", "Florida", "Japan", "Hawaii", "Australia");
        addData(GEOGRAPHY, "Where are the Andes located?", "South America", "North America", "Africa", "Asia");
        addData(GEOGRAPHY, "Which of the following city is not located in Germany?", "Bern", "Kiel", "Munich", "Hannover");
        addData(GEOGRAPHY, "Which of the following is not a vegetable?", "Casserole", "Brusselsprouts",
                "Fat hen", "Fiddlehead");
        addData(GEOGRAPHY, "Which of the following is not a Scandinavian country?", "Iceland", "Denmark", "Finland", "Norway");
        addData(GEOGRAPHY, "Which of the following is NOT amongst the 5 richest countries in the world?", "USA", "Norway",
                "Qatar", "Singapore");
        addData(GEOGRAPHY, "Which of the following borders more than one country?", "Bangladesh", "Denmark",
                "Portugal", "Canada");

    }

    /*
    * Returns all the data in the form of an ArrayList
     */
    public ArrayList<Question> getData() {
        return questions;
    }

    private void addData(String category, String question, String correctAnswer, String incorrectAnswer1,
                         String incorrectAnswer2, String incorrectAnswer3) {

        questions.add(new Question(category, question, correctAnswer, incorrectAnswer1, incorrectAnswer2, incorrectAnswer3));
    }

}

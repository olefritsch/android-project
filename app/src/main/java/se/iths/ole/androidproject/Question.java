package se.iths.ole.androidproject;

/**
 * Created by Ole on 21/11/15.
 */
public class Question {
    private String category;
    private String question;
    private String[] answers = new String[4];

    /**
     * Creates a new question.
     * @param category the category of the Question
     * @param question the question itself
     * @param correctAnswer the correctAnswer
     * @param incorrectAnswer1 wrong answer choice 1
     * @param incorrectAnswer2 wrong answer choice 2
     * @param incorrectAnswer3 wrong answer choice 3
     */
    public Question(String category, String question, String correctAnswer, String incorrectAnswer1, String incorrectAnswer2,
                    String incorrectAnswer3) {
        this.category = category;
        this.question = question;
        answers[0] = correctAnswer;
        answers[1] = incorrectAnswer1;
        answers[2] = incorrectAnswer2;
        answers[3] = incorrectAnswer3;
    }



    /**
     * Returns all possible answers in the form of a String array
     */
    public String[] getAnswers(){
        return answers;
    }

    /**
     * Returns the category for the question.
     */
    public String getCategory() {
        return category;
    }

    /**
     * Returns the question
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Sets the question
     */
    public void setQuestion(String question) {
        this.question = question;
    }

    /**
     * Checks whether a given answer matches the correct answer
     * @param answer string to be compared
     * @returns true if match, else false
     */
    public boolean checkAnswer(String answer) {
        if(answer.equals(answers[0])) {
            return true;
        }else {
            return false;
        }
    }
}

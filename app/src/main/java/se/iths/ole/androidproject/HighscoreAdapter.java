package se.iths.ole.androidproject;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by fredrik on 2015-11-23.
 */
public class HighscoreAdapter extends CursorAdapter {

    public HighscoreAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    /**
     * Inflates the chosen listView.
     * @return Returns the inflater.
     */
    public View newView(Context context, Cursor cursor, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.list_view, null);
        return v;
    }
    /*
        Sets the values to the listview.
     */
    public void bindView(View view, Context context, Cursor cursor){
        TextView nameText = (TextView)view.findViewById(R.id.name_text);
        TextView scoreText = (TextView)view.findViewById(R.id.score_text);

        String name = cursor.getString(cursor.getColumnIndex(DBHelper.NAME_KEY));
        String score = cursor.getString(cursor.getColumnIndex(DBHelper.HIGHSCORE_KEY));
        nameText.setText(name);
        scoreText.setText(score);
    }
}
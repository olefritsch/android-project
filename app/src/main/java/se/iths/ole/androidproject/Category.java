package se.iths.ole.androidproject;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class Category extends AppCompatActivity {

    private static final String SPORT_STRING = "Sport";
    private static final String MUSIC_STRING = "Music";
    private static final String GAMES_STRING = "Games";
    private static final String RANDOM_STRING = null;
    private static final String GEOGRAPHY_STRING = "Geography";
    private static final String PLAYER_TABLE = "PLAYER";
    private DBHelper dbHelper;
    private ArrayList<String> questionArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.categories_title));
        setContentView(R.layout.activity_category);

        dbHelper = new DBHelper(this);

    }

    public void toGameSport(View view) { toGame(SPORT_STRING); }

    public void toGameMusic(View view) { toGame(MUSIC_STRING); }

    public void toGameGame(View view) { toGame(GAMES_STRING); }

    public void toRandom(View view) { toGame(RANDOM_STRING); }

    public void toGameNature(View view) { toGame(GEOGRAPHY_STRING); }

    public void toGame(String category) {
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra(ResultActivity.CATEGORY_KEY, category);
        startActivity(intent);
        finish();
    }

    public void toGameProfile(View view) {
        //Intent intent = new Intent(this, PlayerCategories.class);

        //Have to fix so that you can't start with too few questions

        Cursor cursor = dbHelper.getAllPlayerData();
        questionArrayList = new ArrayList<String>();

        while(cursor.moveToNext()) {
            questionArrayList.add(cursor.getString(3));
        }

        Log.d("Debug arraySize", String.valueOf(questionArrayList.size()));

        if(questionArrayList.size()<10||questionArrayList.equals(null)) {
            Toast.makeText(Category.this, R.string.too_few_questions_toast_text, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(this, GameActivity.class);
            intent.putExtra(ResultActivity.CATEGORY_KEY, PLAYER_TABLE);
            intent.putExtra(GameActivity.CATEGORY_BOOLEAN, true);
            startActivity(intent);
            finish();
        }
    }
}

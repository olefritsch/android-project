package se.iths.ole.androidproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Questions.db";
    public static final String QUESTION_TABLE = "Questions";
    public static final String PROFILES_TABLE = "Profiles";
    public static final String SPORTS_TABLE = "Sport";
    public static final String MUSIC_TABLE = "Music";
    public static final String GAME_TABLE = "Games";
    public static final String GEOGRAPHY_TABLE = "Geography";
    public static final String PLAYER_TABLE = "Player";
    public static final String ID_KEY = "ID";
    public static final String NAME_KEY = "Name";
    public static final String HIGHSCORE_KEY = "Highscore";
    public static final String CATEGORY_KEY = "Category";
    public static final String QUESTION_KEY = "Question";
    public static final String CORRRECT_ANSWER_KEY = "Answer";
    public static final String INCORRECT_ANSWER_1_KEY = "IncorrectAnswer1";
    public static final String INCORRECT_ANSWER_2_KEY = "IncorrectAnswer2";
    public static final String INCORRECT_ANSWER_3_KEY = "IncorrectAnswer3";
    public static final String PLAYER_QUESTIONS_TABLE = "PlayerQuestions";
    public static final String PLAYER_CATEGORIES_KEY = "PlayerCategoriesKey";
    public static final String PLAYER_QUESTION_KEY = "PlayerQuestion";
    public static final String PLAYER_CORRRECT_ANSWER_KEY = "PlayerCorrectAnswer";
    public static final String PLAYER_INCORRECT_ANSWER_1_KEY = "PlayerIncorrectAnswer1";
    public static final String PLAYER_INCORRECT_ANSWER_2_KEY = "PlayerIncorrectAnswer2";
    public static final String PLAYER_INCORRECT_ANSWER_3_KEY = "PlayerIncorrectAnswer3";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + QUESTION_TABLE + " (";
        sql += "_id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql += CATEGORY_KEY + " VARCHAR(255) NOT NULL, ";
        sql += QUESTION_KEY + " TEXT, ";
        sql += CORRRECT_ANSWER_KEY + " VARCHAR(255) NOT NULL, ";
        sql += INCORRECT_ANSWER_1_KEY + " VARCHAR(255) NOT NULL, ";
        sql += INCORRECT_ANSWER_2_KEY + " VARCHAR(255) NOT NULL, ";
        sql += INCORRECT_ANSWER_3_KEY + " VARCHAR(255) NOT NULL";
        sql += ");";

        String sql2 = "CREATE TABLE " + PROFILES_TABLE + " (";
        sql2 += "_id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql2 += NAME_KEY + " VARCHAR(255) NOT NULL, ";
        sql2 += HIGHSCORE_KEY + " INTEGER";
        sql2 += CATEGORY_KEY + " VARCHAR(255) ";
        sql2 += ");";

        String sql3 = "CREATE TABLE " + PLAYER_QUESTIONS_TABLE + " (";
        sql3 += "_id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql3 += PLAYER_CATEGORIES_KEY + " VARCHAR(255) NOT NULL, ";
        sql3 += PLAYER_QUESTION_KEY + " TEXT, ";
        sql3 += PLAYER_CORRRECT_ANSWER_KEY + " VARCHAR(255) NOT NULL, ";
        sql3 += PLAYER_INCORRECT_ANSWER_1_KEY + " VARCHAR(255) NOT NULL, ";
        sql3 += PLAYER_INCORRECT_ANSWER_2_KEY + " VARCHAR(255) NOT NULL, ";
        sql3 += PLAYER_INCORRECT_ANSWER_3_KEY + " VARCHAR(255) NOT NULL";
        sql3 += ");";

        String sql4 = "CREATE TABLE " + SPORTS_TABLE + " (";
        sql4 += "_id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql4 += NAME_KEY + " VARCHAR(255) NOT NULL, ";
        sql4 += HIGHSCORE_KEY + " INTEGER";
        sql4 += ");";

        String sql5 = "CREATE TABLE " + MUSIC_TABLE + " (";
        sql5 += "_id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql5 += NAME_KEY + " VARCHAR(255) NOT NULL, ";
        sql5 += HIGHSCORE_KEY + " INTEGER";
        sql5 += ");";

        String sql6 = "CREATE TABLE " + GAME_TABLE + " (";
        sql6 += "_id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql6 += NAME_KEY + " VARCHAR(255) NOT NULL, ";
        sql6 += HIGHSCORE_KEY + " INTEGER";
        sql6 += ");";

        String sql7 = "CREATE TABLE " + GEOGRAPHY_TABLE + " (";
        sql7 += "_id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql7 += NAME_KEY + " VARCHAR(255) NOT NULL, ";
        sql7 += HIGHSCORE_KEY + " INTEGER";
        sql7 += ");";

        String sql8 = "CREATE TABLE " + PLAYER_TABLE + " (";
        sql8 += "_id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql8 += NAME_KEY + " VARCHAR(255) NOT NULL, ";
        sql8 += HIGHSCORE_KEY + " INTEGER";
        sql8 += ");";


        db.execSQL(sql);
        db.execSQL(sql2);
        db.execSQL(sql3);
        db.execSQL(sql4);
        db.execSQL(sql5);
        db.execSQL(sql6);
        db.execSQL(sql7);
        db.execSQL(sql8);

        QuestionData data = new QuestionData();
        ArrayList<Question> dataList = data.getData();
        for (int i=0; i<dataList.size(); i++) {
            String[] answers = dataList.get(i).getAnswers();
            addDefaultData(db, dataList.get(i).getCategory(), dataList.get(i).getQuestion(), answers[0], answers[1], answers[2], answers[3]);
        }
    }

    private void addDefaultData(SQLiteDatabase db, String category, String question, String correctAnswer, String incorrectAnswer1,
                                String incorrectAnswer2, String incorrectAnswer3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CATEGORY_KEY, category);
        contentValues.put(QUESTION_KEY, question);
        contentValues.put(CORRRECT_ANSWER_KEY, correctAnswer);
        contentValues.put(INCORRECT_ANSWER_1_KEY, incorrectAnswer1);
        contentValues.put(INCORRECT_ANSWER_2_KEY, incorrectAnswer2);
        contentValues.put(INCORRECT_ANSWER_3_KEY, incorrectAnswer3);

        db.insert(QUESTION_TABLE, null, contentValues);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + QUESTION_TABLE);
        onCreate(db);
    }

    public boolean insertData(String category, String question, String correctAnswer, String incorrectAnswer1,
                              String incorrectAnswer2, String incorrectAnswer3) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CATEGORY_KEY, category);
        contentValues.put(QUESTION_KEY, question);
        contentValues.put(CORRRECT_ANSWER_KEY, correctAnswer);
        contentValues.put(INCORRECT_ANSWER_1_KEY, incorrectAnswer1);
        contentValues.put(INCORRECT_ANSWER_2_KEY, incorrectAnswer2);
        contentValues.put(INCORRECT_ANSWER_3_KEY, incorrectAnswer3);

        long result = db.insert(QUESTION_TABLE, null, contentValues);

        db.close();

        if (result == -1)
            return false;
        else
            return true;

    }

    //Added this to populate database with our questions and answers in the code.
    public boolean insertQuestions(Question question) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CATEGORY_KEY, question.getCategory());
        contentValues.put(QUESTION_KEY, question.getQuestion());
        contentValues.put(CORRRECT_ANSWER_KEY, question.getAnswers()[0]);
        contentValues.put(INCORRECT_ANSWER_1_KEY, question.getAnswers()[1]);
        contentValues.put(INCORRECT_ANSWER_2_KEY, question.getAnswers()[2]);
        contentValues.put(INCORRECT_ANSWER_3_KEY, question.getAnswers()[3]);


        long result = db.insert(QUESTION_TABLE, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;

    }

    public void addProfileToDB(String name) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME_KEY, name);
        contentValues.put(HIGHSCORE_KEY, 0);
        db.insert(PROFILES_TABLE, null, contentValues);
        db.insert(SPORTS_TABLE, null, contentValues);
        db.insert(MUSIC_TABLE, null, contentValues);
        db.insert(GAME_TABLE, null, contentValues);
        db.insert(GEOGRAPHY_TABLE, null, contentValues);
        db.insert(PLAYER_TABLE, null, contentValues);
        db.close();
    }

    public Cursor getProfiles() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + PROFILES_TABLE + ";", null);
    }

    public ArrayList<String> getProfileNames() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<String> profileNames = new ArrayList<String>();
        Cursor cursor = db.query(PROFILES_TABLE, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            int nameColumn = cursor.getColumnIndex(NAME_KEY);
            do {
                profileNames.add(cursor.getString(nameColumn));
            } while (cursor.moveToNext());
        }
        db.close();
        return profileNames;
    }

    public void deleteProfile(long id){
        SQLiteDatabase db = getWritableDatabase();
        String string =String.valueOf(id);
        db.execSQL("DELETE FROM " + PROFILES_TABLE + " WHERE _id = '" + string + "'");
        ContentValues contentValues = new ContentValues();
        contentValues.remove(NAME_KEY);
    }

    /**
     *
     * @return
     * returns all premade questions in the database
     */
    public Cursor getAllData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + QUESTION_TABLE, null);
        return res;
    }

    public boolean updateData(String id, String category, String question, String correctAnswer,
                              String incorrectAnswer1, String incorrectAnswer2, String incorrectAnswer3) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(ID_KEY, id);
        contentValues.put(CATEGORY_KEY, category);
        contentValues.put(QUESTION_KEY, question);
        contentValues.put(CORRRECT_ANSWER_KEY, correctAnswer);
        contentValues.put(INCORRECT_ANSWER_1_KEY, incorrectAnswer1);
        contentValues.put(INCORRECT_ANSWER_2_KEY, incorrectAnswer2);
        contentValues.put(INCORRECT_ANSWER_3_KEY, incorrectAnswer3);

        db.update(QUESTION_TABLE,contentValues, "id = ?",new String[] {id});

        return true;
    }

    /*
    Content for highscore list.
     */
    public Cursor getHighscore(String table) {
        SQLiteDatabase db = getReadableDatabase();
        return db.query(table, null,
                null, null, null, null, "Highscore DESC");
    }
    public int getProfileHighscore(String player, String table){
        SQLiteDatabase db = getReadableDatabase();
        String[] searchCriteria = new String[] {player};
        Cursor c = db.query(table, null,
                "Name=?", searchCriteria, null, null, null);
        c.moveToFirst();
        return c.getInt(2);
    }

    public void updateHighscore(String name ,int points, String table){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(HIGHSCORE_KEY, points);
        String[] selectionArgs = new String[]{name};
        db.update(table, cv, NAME_KEY + "=?", selectionArgs);
    }

    /**
     *
     * @param id
     * @return
     * deletes the question at the specified row in the database
     */
    public Integer deleteData(String id){
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(PLAYER_QUESTIONS_TABLE, " _id = ?",new String[] {id});
    }

    /**
     *
     * @param category
     * @param question
     * @param correctAnswer
     * @param incorrectAnswer1
     * @param incorrectAnswer2
     * @param incorrectAnswer3
     * @return
     * Adds a question to the player's question database
     */
    public boolean insertPlayerData(String category, String question, String correctAnswer, String incorrectAnswer1,
                              String incorrectAnswer2, String incorrectAnswer3) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PLAYER_CATEGORIES_KEY, category);
        contentValues.put(PLAYER_QUESTION_KEY, question);
        contentValues.put(PLAYER_CORRRECT_ANSWER_KEY, correctAnswer);
        contentValues.put(PLAYER_INCORRECT_ANSWER_1_KEY, incorrectAnswer1);
        contentValues.put(PLAYER_INCORRECT_ANSWER_2_KEY, incorrectAnswer2);
        contentValues.put(PLAYER_INCORRECT_ANSWER_3_KEY, incorrectAnswer3);

        long result = db.insert(PLAYER_QUESTIONS_TABLE, null, contentValues);
        db.close();
        if (result == -1)
            return false;
        else
            return true;
    }

    /**
     *
     * @return
     * returns a cursor that points at all the player-created questions
     */
    public Cursor getAllPlayerData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + PLAYER_QUESTIONS_TABLE, null);
        return res;
    }

    /*
    public Cursor getPlayerQuestions(String category) {
        SQLiteDatabase db = getReadableDatabase();

        String selection = PLAYER_CATEGORIES_KEY + "=\"" + category + "\"";
        return db.query(PLAYER_QUESTIONS_TABLE, null, selection, null, null, null, null);
    }*/

    /**
     *
     * @param category
     * @return
     * returns a cursor that points at all the premade questions
     */
    public Cursor getQuestions(String category) {
        SQLiteDatabase db = getReadableDatabase();
        /*
        If category is ALL, get questions from all categories.
         */
        if(category==null){
            return db.query(QUESTION_TABLE, null, null, null, null, null, null);
        } else {
            String selection = CATEGORY_KEY + "=\"" + category + "\"";

            Log.d("debug DBHelper", selection);

            Log.d("debug DBHelper2", String.valueOf(db.query(QUESTION_TABLE, null, selection, null, null, null, null)));

            return db.query(QUESTION_TABLE, null, selection, null, null, null, null);
        }
    }




}
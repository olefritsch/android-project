package se.iths.ole.androidproject;

/**
 * Created by Ole on 25/11/15.
 */
public class Game {
    private String player;
    private Question[] questions;
    private int points;
    private int turn;

    /**
     * Creates a new game for a player.
     * @param player the player playing the game
     * @param questions array of questions to be asked
     */
    public Game(String player,Question[] questions) {
        this.player = player;
        this.questions = questions;
        points = 0;
        turn = 0;
    }

    /**
     * Takes a turn for the player. Checks if guess is correct and if so increases points according to remaining time.
     * @param guess the answer guessed by the player
     * @param seconds the time remaining before turn is over
     * @returns true if correct answer, else false
     */
    public boolean takeTurn(String guess, int seconds) {
        boolean correct = questions[turn].checkAnswer(guess);
        if(turn < 10) {
            turn++;
        }

        if(correct) {
            increasePoints(seconds);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns amount of points scored by the player during this game.
     */
    public int getPoints() {
        return points;
    }

    /**
     * Returns name of the player.
     */
    public String getPlayer() {
        return player;
    }

    private void increasePoints(int points) {
        this.points += 10 * points;
    }
}


package se.iths.ole.androidproject;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreateActivity extends AppCompatActivity {

    DBHelper myDb;
    EditText editCategory, editQuestion, editCorrectAnswer, editIncorrectAnswer1,
            editIncorrectAnswer2, editIncorrectAnswer3, editId;
    Button addButton, deleteButton, viewButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.create_title));
        setContentView(R.layout.activity_create);

        myDb = new DBHelper(this);
        editCategory = (EditText) findViewById(R.id.edit_category);
        editQuestion = (EditText) findViewById(R.id.edit_question);
        editCorrectAnswer = (EditText) findViewById(R.id.edit_correct_answer);
        editIncorrectAnswer1 = (EditText) findViewById(R.id.edit_incorrect_answer1);
        editIncorrectAnswer2 = (EditText) findViewById(R.id.edit_incorrect_answer2);
        editIncorrectAnswer3 = (EditText) findViewById(R.id.edit_incorrect_answer3);
        editId = (EditText) findViewById(R.id.edit_id);
        addButton = (Button) findViewById(R.id.add_button);
        deleteButton = (Button) findViewById(R.id.delete_button);
        viewButton = (Button) findViewById(R.id.view_button);
    }

    /**
     * Method to manualy add new questions to the database.
     * @param view
     */
    public void addData(View view) {
        boolean isInserted = myDb.insertPlayerData("Player",
                editQuestion.getText().toString(),
                editCorrectAnswer.getText().toString(), editIncorrectAnswer1.getText().toString(),
                editIncorrectAnswer2.getText().toString(), editIncorrectAnswer3.getText().toString());
        if (isInserted) {
            Toast.makeText(CreateActivity.this, "Question added", Toast.LENGTH_SHORT).show();
            editCategory.setText("");
            editCorrectAnswer.setText("");
            editIncorrectAnswer1.setText("");
            editIncorrectAnswer2.setText("");
            editIncorrectAnswer3.setText("");
            editQuestion.setText("");
        } else
            Toast.makeText(CreateActivity.this, "Failed to insert data", Toast.LENGTH_SHORT).show();
    }

    /**
     * Shows all the questions and answers in the database.
     * @param view
     */
    public void viewData(View view) {
        Cursor res = myDb.getAllPlayerData();
        if(res.getCount() == 0){
            showMessage("Error","No data found");
            return;
        }
        StringBuffer buffer = new StringBuffer();
        while(res.moveToNext()){
            buffer.append("Id: " + res.getString(0)+ "\n");
            buffer.append("Category: " + res.getString(1)+ "\n");
            buffer.append("Question: " + res.getString(2)+ "\n");
            buffer.append("CorrectAnswer: " + res.getString(3)+ "\n");
            buffer.append("IncorrectAnswer1: " + res.getString(4)+ "\n");
            buffer.append("IncorrectAnswer2: " + res.getString(5)+ "\n");
            buffer.append("IncorrectAnswer3: " + res.getString(6)+ "\n\n");
        }
        showMessage("Data", buffer.toString());
    }

    public void showMessage(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    public void updateMethod(View view) {
        boolean isUpdated = myDb.updateData(editId.getText().toString(), editCategory.getText().toString(),
                editQuestion.getText().toString(), editCorrectAnswer.getText().toString(),
                editIncorrectAnswer1.getText().toString(), editIncorrectAnswer2.getText().toString(),
                editIncorrectAnswer3.getText().toString());

        if(isUpdated) {
            Toast.makeText(CreateActivity.this, "Data updated", Toast.LENGTH_SHORT).show();
            editCategory.setText("");
            editCorrectAnswer.setText("");
            editIncorrectAnswer1.setText("");
            editIncorrectAnswer2.setText("");
            editIncorrectAnswer3.setText("");
            editQuestion.setText("");
        } else
            Toast.makeText(CreateActivity.this,"Data not updated", Toast.LENGTH_SHORT).show();
    }

    /**
     * A method to manualy delete a single row in database.
     * @param view
     */
    public void deleteMethod(View view) {
        Integer deletedRows = myDb.deleteData(editId.getText().toString());
        if(deletedRows > 0){
            Toast.makeText(CreateActivity.this, "Data deleted", Toast.LENGTH_SHORT).show();
            editId.setText("");
        } else {
            Toast.makeText(CreateActivity.this, "Data not deleted", Toast.LENGTH_SHORT).show();
            editId.setText("");
        }
    }
}

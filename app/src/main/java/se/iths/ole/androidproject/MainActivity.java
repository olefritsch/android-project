package se.iths.ole.androidproject;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private boolean hasProfile = false;
    private DBHelper dbHelper;
    private String activeProfile;
    private TextView profileTV;
    private int profilePosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.menu_title));
        setContentView(R.layout.activity_main);
        ProfileHelper.setContext(this);
    }

    public void create(View view) { startActivity(new Intent(this, CreateActivity.class)); }

    public void about(View view) { startActivity(new Intent(this, AboutActivity.class)); }

    public void toProfiles(View view) {
        Intent profileIntent = new Intent(this, ProfilesActivity.class);
        profileIntent.putExtra(ProfilesActivity.BACK_TO_MAIN_KEY, true);
        startActivityForResult(profileIntent, ProfilesActivity.PROFILES_REQUEST_CODE);
    }

    public void highscore(View view) { startActivity(new Intent(this, HighscoreActivity.class));}

    /*
    If profile chosen. Start next activity.
     */
    public void game(View view) {
        if(hasProfile==false) {
            Intent profileIntent = new Intent(this, ProfilesActivity.class);
            startActivity(profileIntent);
        } else {
            Intent intent = new Intent(this, NumberOfPlayersActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==ProfilesActivity.PROFILES_REQUEST_CODE) {
            try {
                hasProfile = true;
                activeProfile = ProfileHelper.getInstance().getActiveProfile();
                profileTV = (TextView) findViewById(R.id.active_profile_text_view);
                profileTV.setText(activeProfile);
                Toast.makeText(MainActivity.this, activeProfile + " is now active", Toast.LENGTH_SHORT).show();
            } catch (NullPointerException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "No Profile chosen", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void toSendMessage(View view) {
        startActivity(new Intent(this, MessageActivity.class));
    }
}

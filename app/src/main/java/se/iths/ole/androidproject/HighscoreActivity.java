package se.iths.ole.androidproject;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class HighscoreActivity extends AppCompatActivity {

    private ListView listview;
    private TextView headline;
    private DBHelper db;
    private HighscoreAdapter adapter;

    private static final String PROFILES_TABLE = "Profiles";
    private static final String SPORTS_TABLE = "Sport";
    private static final String MUSIC_TABLE = "Music";
    private static final String GAME_TABLE = "Games";
    private static final String GEOGRAPHY_TABLE = "Geography";
    private static final String PLAYER_TABLE = "Player";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.highscore_title));
        setContentView(R.layout.activity_highscore);

        headline = (TextView)findViewById(R.id.headline);
            headline.setText(R.string.all_categories);

        db = new DBHelper(this);
        listview = (ListView) findViewById(R.id.highscore);
        Cursor c = db.getHighscore(PROFILES_TABLE);
        adapter = new HighscoreAdapter(this, c);
        listview.setAdapter(adapter);
    }

    /**
     * Method that takes player back to the main menu.
     * @param view
     */
    public void main(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    /**
     * Switches through the different categories of highscorelist.
     * @param view
     */
    public void nextHighscore(View view) {
        db = new DBHelper(this);
        listview = (ListView) findViewById(R.id.highscore);

        if (headline.getText() == SPORTS_TABLE) {
            headline.setText(MUSIC_TABLE);
            Cursor c = db.getHighscore((MUSIC_TABLE));
            adapter = new HighscoreAdapter(this, c);
            listview.setAdapter(adapter);
        } else if (headline.getText() == MUSIC_TABLE) {
            headline.setText((GAME_TABLE));
            Cursor c = db.getHighscore(GAME_TABLE);
            adapter = new HighscoreAdapter(this, c);
            listview.setAdapter(adapter);
        } else if (headline.getText() == GAME_TABLE) {
            headline.setText((GEOGRAPHY_TABLE));
            Cursor c = db.getHighscore(GEOGRAPHY_TABLE);
            adapter = new HighscoreAdapter(this, c);
            listview.setAdapter(adapter);
        } else if (headline.getText() == GEOGRAPHY_TABLE) {
            headline.setText((PLAYER_TABLE));
            Cursor c = db.getHighscore(PLAYER_TABLE);
            adapter = new HighscoreAdapter(this, c);
            listview.setAdapter(adapter);
        }  else if (headline.getText() == PLAYER_TABLE) {
            headline.setText((R.string.all_categories));
            Cursor c = db.getHighscore(PROFILES_TABLE);
            adapter = new HighscoreAdapter(this, c);
            listview.setAdapter(adapter);
        } else {
            headline.setText(SPORTS_TABLE);
            Cursor c = db.getHighscore(SPORTS_TABLE);
            adapter = new HighscoreAdapter(this, c);
            listview.setAdapter(adapter);
        }
    }
}
package se.iths.ole.androidproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    public static final String PLAYER_1_SCORE_KEY = "Player1_Score";
    public static final String PLAYER_2_SCORE_KEY = "Player2_Score";
    public static final String TIME_PASSED_KEY = "TIME_PASSED";
    public static final String CORRECT_GUESSES_KEY = "CORRECT_GUESSES";
    public static final String NEW_HIGHSCORE_KEY = "NEW_HIGHSCORE";
    public static final String CATEGORY_KEY = "CATEGORY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.result_title));
        setContentView(R.layout.activity_result);

        Intent intent = getIntent();
        int player1 = intent.getIntExtra(PLAYER_1_SCORE_KEY, 0);

        TextView headLineTV = (TextView) findViewById(R.id.headline);
        TextView highscoreTV = (TextView) findViewById(R.id.new_highscore);
        TextView scoreTV1 = (TextView)findViewById(R.id.score);
        TextView timeTV1 = (TextView)findViewById(R.id.time_text);
        TextView guessesTV1 = (TextView)findViewById(R.id.guesses_text);

        headLineTV.setText(intent.getStringExtra(CATEGORY_KEY));

        if(intent.hasExtra(NEW_HIGHSCORE_KEY)) {
            highscoreTV.setText(R.string.new_highscore_txt);
        }

        if(ProfileHelper.getInstance().isMpGame()) {
            TextView scoreTV2 = (TextView)findViewById(R.id.points);
            int player2 = intent.getIntExtra(PLAYER_2_SCORE_KEY, 0);

            if(player1 > player2) {
                highscoreTV.setText(getString(R.string.winner_is) + ProfileHelper.getInstance().getActiveProfile());
            }else if (player2 > player1) {
                highscoreTV.setText(getString(R.string.winner_is) + ProfileHelper.getInstance().getActiveProfile2());
            }else {
                highscoreTV.setText(R.string.its_a_tie);
            }

            scoreTV1.setText(ProfileHelper.getInstance().getActiveProfile() + ": " + player1);
            scoreTV2.setText(ProfileHelper.getInstance().getActiveProfile2() + ": " + player2);
        }else {
            int time = intent.getIntExtra(TIME_PASSED_KEY, 0);
            scoreTV1.setText(getString(R.string.score_txt) + player1);
            if(time < 5) {
                timeTV1.setText(getString(R.string.time_txt) + getString(R.string.fast_furious));
            }else {
                timeTV1.setText(getString(R.string.time_txt) + time);
            }

            guessesTV1.setText(getString(R.string.correct_guesses_txt) + intent.getIntExtra(CORRECT_GUESSES_KEY, 0));
        }
    }
    /*
    Goes back to main menu!
     */
    public void main(View view) {
        ProfileHelper.getInstance().setMpGameFalse();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}

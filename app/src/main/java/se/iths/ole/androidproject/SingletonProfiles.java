package se.iths.ole.androidproject;

/**
 * Created by iths on 09/12/15.
 */
public class SingletonProfiles {
    private Profile activeProfile;
    private DBHelper db;

    private static SingletonProfiles instance;

    public SingletonProfiles getInstance() {
        if(instance != null) {
            return instance;
        }else {
            return new SingletonProfiles();
        }
    }



    public Profile getActiveProfile() {
        return activeProfile;
    }

}

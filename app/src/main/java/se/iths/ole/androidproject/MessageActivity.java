package se.iths.ole.androidproject;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class MessageActivity extends AppCompatActivity {

    private ListView questionListView;
    private DBHelper dbHelper;
    private Question question;
    private String message;
    private String[] answers;
    private ArrayList<String> questions;
    public static final int CHOOSE_CONTACT = 1;
    private TextView contactTextView;
    private Cursor cursor;
    private String number;
    private String name;
    private TextView numberTextVeiw;
    private String cNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        questionListView = (ListView) findViewById(R.id.question_list_view);
        contactTextView = (TextView) findViewById(R.id.contact_name_text_view);
        numberTextVeiw = (TextView) findViewById(R.id.contact_number_text_view);
        dbHelper = new DBHelper(this);
        number="";

        fixList();

    }

    /**
     *
     * @param view
     * Opens the contact list, allowing the player to choose which contact to send the question to
     */
    public void chooseContact(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, CHOOSE_CONTACT);
    }

    /**
     *
     * @param reqCode
     * @param resultCode
     * @param data
     * Here, the contact's name and number is set to the textviews. The number can now be used to send the question
     */
    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (CHOOSE_CONTACT):
                if(resultCode== Activity.RESULT_OK) {

                    Uri contactData = data.getData();
                    Cursor contactCursor = managedQuery(contactData, null, null, null, null);
                    if(contactCursor.moveToFirst()) {
                        String id = contactCursor.getString(contactCursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                        String hasPhone = contactCursor.getString(contactCursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if(hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = " + id,
                                    null, null);
                            phones.moveToFirst();
                            number = phones.getString(phones.getColumnIndex("data1"));
                            System.out.println("number is: "+ cNumber);
                        }
                        String name = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        contactTextView.setText(name);
                        numberTextVeiw.setText(number);
                    }
                }
                break;
        }
    }

    private void fixList() {

        final Cursor questionCursor = dbHelper.getAllData();
        questions = new ArrayList<String>();

        while (questionCursor.moveToNext()) {
            questions.add(questionCursor.getString(2));
        }

        ArrayAdapter<String> questionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, questions);
        questionListView.setAdapter(questionAdapter);

        questionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(questionCursor.moveToFirst()) {
                    questionCursor.moveToPosition(position);
                    question = new Question(questionCursor.getString(1),
                                            questionCursor.getString(2),
                                            questionCursor.getString(3),
                                            questionCursor.getString(4),
                                            questionCursor.getString(5),
                                            questionCursor.getString(6));
                }

                if(number.equals(null)) {
                    Toast.makeText(MessageActivity.this, "Choose contact", Toast.LENGTH_SHORT).show();
                } else if (number.length()<10) {
                    Toast.makeText(getApplicationContext(), "Invalid phone number", Toast.LENGTH_SHORT).show();
                } else {
                    sendQuestion(question, number);
                }

            }
        });

    }


    /**
     *
     * @param question
     * @param number
     * Sets up a question based and sends it to the specified number
     */
    protected void sendQuestion(Question question, String number) {

        message = question.getQuestion();
        answers = question.getAnswers();
        for(int i=0; i<4; i++) {
            message += "\"" +"[x]" + answers[i];
        }

        try{
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(number, null, message, null, null);
            Toast.makeText(getApplicationContext(), "Question sent.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Question not sent", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }
}

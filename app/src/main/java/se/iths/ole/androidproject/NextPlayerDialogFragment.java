package se.iths.ole.androidproject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;

/**
 * Created by Ole on 14/12/15.
 */
public class NextPlayerDialogFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        GameActivity.timer.cancel();
        builder.setMessage("Please pass the phone to " + GameActivity.nextPlayer)
                .setPositiveButton(R.string.play, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        GameActivity.timer.start();
                    }
                });
        return builder.create();
    }
}


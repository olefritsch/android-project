package se.iths.ole.androidproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class NumberOfPlayersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.gamemodes_title));
        setContentView(R.layout.activity_number_of_players);
    }

    public void startSPGame(View view) {
        Intent intent = new Intent(this, Category.class);
        intent.putExtra(ProfilesActivity.FROM_NUMBER_OF_PLAYERS_KEY, false);
        startActivity(intent);
        finish();
    }

    public void startMPGame(View view) {
        Intent intent = new Intent(this, ProfilesActivity.class);
        intent.putExtra(ProfilesActivity.FROM_NUMBER_OF_PLAYERS_KEY, true);
        startActivity(intent);
        finish();
    }
}